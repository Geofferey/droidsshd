/**
 * 
 */
package br.com.bott.droidsshd.activity;

import java.io.File;
import br.com.bott.droidsshd.R;
import br.com.bott.droidsshd.system.Base;
import br.com.bott.droidsshd.system.Util;
import br.com.bott.droidsshd.tools.ListenPortPicker;
import br.com.bott.droidsshd.tools.RemoteServerPortPicker;
import br.com.bott.droidsshd.tools.LocalPortPicker;
import br.com.bott.droidsshd.tools.RemotePortPicker;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.util.Log;

/**
 * @author mestre
 *
 */
public class Preferences extends PreferenceActivity 
	implements OnSharedPreferenceChangeListener {
	private static final String TAG = "DroidSSHd-Prefs";
	private CheckBoxPreference mDebug;
//	private EditTextPreference mPort;
	private ListenPortPicker listenPort;
	private LocalPortPicker localPort;
	private EditTextPreference remAddr;
	private EditTextPreference remUser;
	private RemoteServerPortPicker remSrvPort;
	private Preference mAuthorizedKeys;
	private Preference mPrivateKey;
	private RemotePortPicker remotePort;
	private String fileChooser;

	@Override
	protected void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		addPreferencesFromResource(R.xml.preferences);
		mDebug = (CheckBoxPreference) findPreference(getString(R.string.pref_debug_key));
		if (mDebug.isChecked()) {
			Log.v(TAG, "onCreate called");
		}
//		mPort = (EditTextPreference) findPreference(getString(R.string.pref_dropbear_port_key));
//		mPort.getEditText().setKeyListener(new DialerKeyListener());
        listenPort = (ListenPortPicker) findPreference(getString(R.string.pref_dropbear_port_key));
        localPort = (LocalPortPicker) findPreference(getString(R.string.pref_forward_local_port_key));
		remotePort = (RemotePortPicker) findPreference(getString(R.string.pref_forward_remote_port_key));
        remAddr = (EditTextPreference) findPreference(getString(R.string.pref_remote_server_key));
		remUser = (EditTextPreference) findPreference(getString(R.string.pref_remote_username_key));
        remSrvPort = (RemoteServerPortPicker) findPreference(getString(R.string.pref_remote_server_port_key));
        
		mAuthorizedKeys = findPreference("authorized_keys_key");
		mAuthorizedKeys.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
			    fileChooser = "PubKey";
				Intent p = new Intent(getBaseContext(), com.h3r3t1c.filechooser.FileChooser.class);
				p.putExtra("path", "/sdcard/");
//				p.putExtra("path", "/mnt");
				startActivityForResult(p, R.string.activity_file_chooser);
				return true;
			}
		});
		
		mPrivateKey = findPreference("private_key_key");
		mPrivateKey.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			public boolean onPreferenceClick(Preference preference) {
			    fileChooser = "PrivKey";
				Intent p = new Intent(getBaseContext(), com.h3r3t1c.filechooser.FileChooser.class);
				p.putExtra("path", "/sdcard/");
//				p.putExtra("path", "/mnt");
				startActivityForResult(p, R.string.activity_file_chooser);
				return true;
			}
		});
	}
	

	@Override
	protected void onResume() {
		super.onResume();
		if (mDebug.isChecked()) {
			Log.v(TAG, "onResume called");
		}
		getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
//		mPort.setSummary(getString(R.string.pref_dropbear_port_summary) + " " + mPort.getText());
		listenPort.setSummary(getString(R.string.pref_dropbear_port_summary) + " " + listenPort.getValue());
		localPort.setSummary(getString(R.string.pref_forward_local_port_summary) + " " + localPort.getValue());
		remotePort.setSummary(getString(R.string.pref_forward_remote_port_summary) + " " + remotePort.getValue());
		remAddr.setSummary(getString(R.string.pref_remote_server_summary) + " " + remAddr.getText());
		remUser.setSummary(getString(R.string.pref_remote_username_summary) + " " + remUser.getText());
		remSrvPort.setSummary(getString(R.string.pref_remote_server_port_summary) + " " + remSrvPort.getValue());
		
	}

	@Override
	protected void onPause() {
		super.onPause();
		getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);	
	}

	@Override
	public void onBackPressed() {
		Intent i = new Intent(this, br.com.bott.droidsshd.DroidSSHd.class);
		i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		startActivity(i);
//		super.onBackPressed();
	}

	public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
		if(mDebug.isChecked()) {
			Log.d(TAG, "onSharedPreferenceChanged called with key " + key);
		}

		if (key.equals(getString(R.string.pref_dropbear_port_key))) {
			listenPort.setSummary(getString(R.string.pref_dropbear_port_summary) + " " + listenPort.getValue());
		}
		if (key.equals(getString(R.string.pref_forward_local_port_key))) {
			localPort.setSummary(getString(R.string.pref_forward_local_port_summary) + " " + localPort.getValue());
		}
		if (key.equals(getString(R.string.pref_forward_remote_port_key))) {
			remotePort.setSummary(getString(R.string.pref_forward_remote_port_summary) + " " + remotePort.getValue());
		}
		if (key.equals(getString(R.string.pref_remote_server_key))) {
			remAddr.setSummary(getString(R.string.pref_remote_server_key) + " " + remAddr.getText());
		}
		if (key.equals(getString(R.string.pref_remote_username_key))) {
			remUser.setSummary(getString(R.string.pref_remote_username_key) + " " + remUser.getText());
		}
		if (key.equals(getString(R.string.pref_remote_server_port_key))) {
			remSrvPort.setSummary(getString(R.string.pref_remote_server_port_summary) + " " + remSrvPort.getValue());
		}
		Util.doRun("/system/exbin/update-droidsshd", true, null);	
//		if (key.equals("authorized_keys_key")){
//			Log.v(TAG, "authorized_keys_key changed: " + key);
//		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(mDebug.isChecked()) {
			if (data!=null) {
				Log.d(TAG, "onActivityResult(" + requestCode + ", " + resultCode + ", " + data.toString() + ") called");
			} else {
				Log.d(TAG, "onActivityResult(" + requestCode + ", " + resultCode + ", null) called");
			}
		}
		if(resultCode==RESULT_OK && fileChooser == "PubKey") {
			if(mDebug.isChecked()){
				Log.v(TAG,"Importing public key...");
				Log.v(TAG,"path = " + data.getStringExtra("path"));
			}
			Base.refresh();
			File tmp = new File(data.getStringExtra("path"));
			if(tmp.length()<1024) {
				Util.doRun("mount -o rw,remount /system; " + "cp " + data.getStringExtra("path") + " " + Base.getDropbearAuthorizedKeysFilePath() + ";" + " " + "chmod 600 " + Base.getDropbearAuthorizedKeysFilePath() + ";" + " " + "mount -o ro,remount /system", true, null);
//				Util.doRun("mount -o rw,remount /system", true, null);
//				Util.copyFile(data.getStringExtra("path"), Base.getDropbearAuthorizedKeysFilePath());
//				Util.chmod(Base.getDropbearAuthorizedKeysFilePath(), 0600);
//				Util.doRun("mount -o ro,remount /system", true, null);
				Util.showMsg("Public key copied");
			} else {
				Util.showMsg("The file is too big to be a public key.");
			}
		}
		
		if(resultCode==RESULT_OK && fileChooser == "PrivKey") {
			if(mDebug.isChecked()){
				Log.v(TAG,"Importing remote private key...");
				Log.v(TAG,"path = " + data.getStringExtra("path"));
			}
			Base.refresh();
			File tmp = new File(data.getStringExtra("path"));
			if(tmp.length()<2048) {
				Util.doRun("/system/exbin/import-pk " + data.getStringExtra("path"), true, null);
//				Util.doRun("mount -o rw,remount /system; " + "cp " + data.getStringExtra("path") + " " + Base.getRemoteSrvPrivKeyFilePath() + ";" + " " + "chmod 600 " + Base.getRemoteSrvPrivKeyFilePath() + ";" + " " + "mount -o ro,remount /system", true, null);
//				Util.doRun("cp " + data.getStringExtra("path") + " " + Base.getRemoteSrvPrivKeyFilePath(), true, null);
//              Util.doRun("chmod 600 " + Base.getRemoteSrvPrivKeyFilePath(), true, null);
//				Util.doRun("mount -o ro,remount /system", true, null);
				Util.showMsg("Remote private key copied");
			} else {
				Util.showMsg("The file is too big to be a private key.");
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
}
